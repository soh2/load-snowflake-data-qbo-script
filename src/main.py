from botocore.exceptions import ClientError
import boto3
import os
import json
import pika
import uuid
import logging
from boto3.dynamodb.conditions import Key
import datetime


logger = logging.getLogger()
logger.setLevel(logging.INFO)

dynamo_resource = None

# Rabbitmq setup
credentials = pika.PlainCredentials(
    os.getenv("RABBITMQ_USER"),
    os.getenv("RABBITMQ_PASSWORD")
)

connection = pika.BlockingConnection(
    pika.ConnectionParameters(host=os.getenv("RABBITMQ_HOST"), credentials=credentials)
)

channel = connection.channel()
exchange_name = os.getenv("RABBITMQ_EXCHANGE_NAME", 'qbo_sync')
channel.exchange_declare(exchange=exchange_name, exchange_type='fanout')

# 1. Read from the dynamodb catalog, filter based on last_updated after the date (nov 3rd) and 'qbo' or 'xero' in key
def get_dynamo_resource():
    global dynamo_resource
    if dynamo_resource is None:
        dynamo_resource = boto3.resource("dynamodb", "us-east-1")
    return dynamo_resource

# catalog set up
dynamodb = get_dynamo_resource()
catalog = dynamodb.Table("catalog")
date_cutoff = '2021-11-03 00:00:00'

# filter on origin is qbo or xero and last_updated is > Nov 2nd.
query_kwargs = {
    'IndexName': 'origin-key-index',
    'KeyConditionExpression': Key('origin').eq('qbo'),
    'FilterExpression': Key('last_updated').gte(date_cutoff),
    'ProjectionExpression': "#ky, last_updated, origin",
    'ExpressionAttributeNames': {"#ky": "key"}
}

done = False
start_key = None
chunked_items = []
while not done:
    if start_key:
        query_kwargs['ExclusiveStartKey'] = start_key
    response = catalog.query(**query_kwargs)
    items = response.get('Items', [])
    logger.info(f'received {len(items)} relevant responses from recent batch of dynamo catalog scan')
    chunked_items.extend(items)

    start_key = response.get('LastEvaluatedKey', None)
    done = start_key is None

    # Chunk messages in groups of approximately 1000
    if len(chunked_items) < 1000:
        continue
    else:
        message_id = str(uuid.uuid4())
        job_id = str(uuid.uuid4())
        event = {
            "time_started": datetime.datetime.now().timestamp(),
            "time_ended": datetime.datetime.now().timestamp(),
            "event_type": 'qbo_sync',
            "type": 'qbo_sync',
            "job_id": job_id,
            "items": chunked_items,
            "metadata": {
                "type": 'qbo_sync_backfill'
            }
        }

        channel.basic_publish(
            exchange=exchange_name,
            routing_key="",
            body=json.dumps(event),
            properties=pika.BasicProperties(
                delivery_mode=2,  # make message persistent
                message_id=message_id,
                headers={"requeue_count": 0}
            )
        )
        chunked_items = []

    



# {"time_started": 1639475880.621418, "time_ended": 1639537642.064856, "event_type": "workpuls_sync", "type": "workpuls_sync", "job_id": "05d5705c-91f9-434f-a2b5-edbba8f00653", "items": ["workpuls/fragment/7a8bd535-a95e-42f1-8c6e-e4676838c6a2", "workpuls/fragment/6f661a12-3992-4c16-8cd4-5749376ea321", "workpuls/fragment/a04213ee-0e77-4019-bd8d-d2f705479e83", "workpuls/fragment/a6ea2a0d-bd04-4d46-a0a7-f6d89938a6c3", "workpuls/fragment/d0595362-48ba-4b97-a352-fae5e81a5174", "workpuls/fragment/8eadcdd1-72c2-43ad-a185-86a53959d1c4", "workpuls/fragment/81a612b2-6f3a-447d-a56a-6e017bb66014", "workpuls/fragment/5d8e1afc-1e1c-438a-8a52-2d076a03fa49", "workpuls/fragment/71b039f7-8267-4a63-bb3a-ec6a40ba1659", "workpuls/fragment/eb647f3c-6bd2-4e40-a4d0-e1f812be573c", "workpuls/fragment/d750f65b-b43c-4698-b6dd-69460fe2c9ce", "workpuls/fragment/b66ba586-684e-402e-9965-4baff90a1e33", "workpuls/fragment/6f7c0ec2-f050-4210-aa3e-82b4245a9d9d", "workpuls/fragment/7dd6a902-67a5-489e-b1f0-c9a44e6e1123", "workpuls/fragment/3487abcc-348e-4311-bae7-cd82ed18307f", "workpuls/fragment/c106ef2e-4d4e-4371-9ad6-3a22973aabd9", "workpuls/fragment/14bd2e73-b943-4c4a-8580-d712261ae9ea", "workpuls/fragment/ff701b2e-0059-4379-93e9-9cfaba9d7151", "workpuls/fragment/09c09950-bc64-4550-8f89-a7b85cafcf23", "workpuls/fragment/b7dcadf0-3ef7-4fc7-a909-0852c2e0b6a0", "workpuls/fragment/91606016-0e7f-42b8-a87c-7b7d12b4b0ac", "workpuls/fragment/2d5e6a5c-8007-4036-b6f1-d9e4922e8d21", "workpuls/fragment/1ec8dfd8-2df4-4921-afe1-cf7444d2a8d7", "workpuls/fragment/f114c600-3991-4908-b2db-6782fd67e3c4", "workpuls/fragment/58b20dbd-017c-4f38-b621-02129412c4b5", "workpuls/fragment/7c10df78-b412-4e6b-b8fb-e5a141626bf6", "workpuls/fragment/6fe433f0-e30d-4ab5-9053-dd3aed46512b", "workpuls/fragment/0ca3ac33-f26c-4fb2-8db1-02d7902f6eac", "workpuls/fragment/a3b46129-0695-445e-9f53-7d1dd225a177", "workpuls/fragment/6a32bc48-2696-4c5b-a106-b277bf0eace6", "workpuls/fragment/2e811a23-976f-419c-b5e2-97089553d310", "workpuls/fragment/e4765214-4d36-4bf3-b44f-0aa305a55797", "workpuls/fragment/2d5257f3-3810-40e5-8126-7b07e48020a7", "workpuls/fragment/393cb10e-e10d-44ae-a3f4-4a34fa674b59", "workpuls/fragment/278492ec-e19c-4292-96dc-6f06b2be3c74", "workpuls/fragment/12a512c9-fbc3-46de-9f21-947f39ba2d21", "workpuls/fragment/ca6c9939-476c-4f3c-a6c6-9ef30c5be8d7", "workpuls/fragment/80986e0c-21c1-438f-b76f-007cfb1ba441", "workpuls/fragment/15c2a5fc-c6dc-4bbb-a46c-47e93dfd4a8e", "workpuls/fragment/139aa230-00bb-4743-be9e-3f3af981ce8c", "workpuls/fragment/cfdedf3e-8dce-4939-892d-95dd02b03b1a", "workpuls/fragment/392388d9-1c3d-44bc-8e3f-d2d98c7fec4a", "workpuls/fragment/59bec04a-06b5-4cc2-bbb9-2881a827217c", "workpuls/fragment/a5cd0936-3fe5-4f53-b637-989e426f3962", "workpuls/fragment/1c4a2aaf-5d65-4848-8b49-47106584130e", "workpuls/fragment/7f1ca8af-1499-4a57-9149-60c71da8d3d5", "workpuls/fragment/5889b9b6-d010-4921-9045-cca2fb679fdd", "workpuls/fragment/9998b541-179d-4311-a8af-3cfcbc2259eb", "workpuls/fragment/6bbb01e4-6747-4e6c-972c-8a26e3622d25", "workpuls/fragment/5d9c9996-5e5e-442c-81b7-8570f8eff689"], "metadata": {"origin": "workpuls", "object_type": "fragment"}}
