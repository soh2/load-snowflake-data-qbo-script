FROM 541868749621.dkr.ecr.us-east-1.amazonaws.com/python:3.9.2

ENV PYTHONUNBUFFERED=0

COPY Pipfile Pipfile.lock ./
RUN pip install pipenv && pipenv install --system --ignore-pipfile --deploy

COPY src/ /app

WORKDIR /app
ENV PYTHONPATH "/app"

ENTRYPOINT ["python", "/app/main.py"]
